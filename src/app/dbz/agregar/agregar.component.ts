import { Component, Input } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
})
export class AgregarComponent {

  @Input() personajes: Personaje[] = [];

  @Input() nuevopj: Personaje = {
    nombre: '',
    poder: 0
  }

  agregar(){
    if(this.nuevopj.nombre.trim().length === 0)  return;
    // console.log( this.nuevopj )
    this.personajes.push( this.nuevopj )
    this.nuevopj = {
      nombre: '',
      poder: 0
    }
    // console.log( this.personajes )
  }
}
