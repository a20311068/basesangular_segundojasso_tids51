import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common"
import { HeroeComponent } from "./heroe/heroe.component";
import { ListadoComponent } from "./listado/listado.component";

@NgModule({
    declarations: [     // Las declaraciones dicen cuáles componentes contienen este módulo
        HeroeComponent,
        ListadoComponent
    ],
    exports: [          // Los exports indican cuáles componentes serán públicos.
        ListadoComponent 
    ],
    imports: [          //En imports van los módulos
        CommonModule
    ]
})

export class HeroesModule{ }
