import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common"
import { ContadorComponent } from "./contador/contador.component";

@NgModule({
    declarations: [     // Las declaraciones dicen cuáles componentes contienen este módulo
        ContadorComponent
    ],
    exports: [          // Los exports indican cuáles componentes serán públicos.
        ContadorComponent
    ],
    imports: [          //En imports van los módulos
        CommonModule
    ]
})

export class ContadorModule{ }
